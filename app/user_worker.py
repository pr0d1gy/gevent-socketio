import re

from app.redis_client import RedisClient


class UserWorker(object):

    REGEXP_IS_VARS_MSG = re.compile('(\s([\w]+)='
                                    '(\'[^\']+\'|\"[^\"]+\"|[^\s]+))')

    def __init__(self, namespace):
        self.namespace = namespace
        self.redis_client = RedisClient()

    @property
    def nickname(self):
        raise NotImplemented

    @property
    def uid(self):
        raise NotImplemented

    @property
    def redis_vars_token(self):
        return 'user:{uid}:vars'.format(uid=self.uid)

    @property
    def is_member(self):
        return self.redis_client.exist_in('users', self.uid)

    def register(self):
        self.redis_client.save_in('users', self.uid, self.nickname)

    @staticmethod
    def get_nickname_by_uid(uid):
        if not uid:
            return None

        return RedisClient().get_from('users', uid)

    def __send_vars_from_message(self, msg):
        def send_var(key, value):
            """
                Send private message to User with sockets.
                Example:
                    @key: value
            """
            self.namespace.emit(
                'msg_to_room',
                '@%s' % key,
                value
            )

        set_of_vars = set([m.strip() for m in msg.split(' ')[1:]])
        if not set_of_vars:
            self.namespace.send_to_room(msg=msg)

            return None

        session_vars = self.namespace.session.get('vars', {})
        for var in set_of_vars:
            send_var(key=var, value=session_vars.get(var, ''))

    def __set_vars_from_message(self, vars_set):
        session_vars = self.namespace.session.get('vars', {})
        for var in vars_set:
            if len(var) != 3:
                continue

            session_vars[var[1]] = var[2]

        self.namespace.session['vars'] = session_vars
        self.__update_vars_in_redis()

    def __update_vars_in_redis(self):
        self.redis_client.save(
            key=self.redis_vars_token,
            value=self.namespace.session.get('vars', {}),
            is_pack=True
        )

    def __process_vars_message(self, msg):
        msg_vars_to_set = self.REGEXP_IS_VARS_MSG.findall(msg)
        if msg_vars_to_set:
            self.__set_vars_from_message(vars_set=msg_vars_to_set)

        else:
            self.__send_vars_from_message(msg=msg)

    def process_message(self, msg):
        """
            If `vars` in message - process message and
            sent result to current user.

            Else sent message to room.
        """
        if msg.startswith('vars '):
            self.__process_vars_message(msg=msg)
        else:
            self.namespace.send_to_room(msg)

    def update_vars_from_redis(self):
        """
            Update session `vars` from redis.
        """
        self.namespace.session['vars'] = \
            self.redis_client.get(
                key=self.redis_vars_token,
                is_unpack=True
            ) or {}

    def close(self, is_clear=True):
        if is_clear:
            self.redis_client.delete(
                key=self.redis_vars_token
            )
