import settings

import msgpack

from redis import Redis


class RedisClient(object):

    def __init__(self):
        self.__r = Redis(
            host=settings.REDIS_HOST,
            port=settings.REDIS_PORT,
            db=settings.REDIS_DB
        )

    def exist_in(self, name, key):
        return bool(self.__r.hexists(name, key))

    def save_in(self, name, key, value):
        self.__r.hset(name, key, value)

    def get_from(self, name, key):
        return self.__r.hget(name, key)

    def get(self, key, is_unpack=True):
        value = self.__r.get(key)

        if is_unpack and \
                value:
            return msgpack.unpackb(value)

        return value

    def save(self, key, value, is_pack=True):
        if is_pack and \
                value:
            value = msgpack.packb(value)

        self.__r.set(key, value)

    def delete(self, key):
        self.__r.delete(key)
