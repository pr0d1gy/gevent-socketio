import settings

from socketio.namespace import BaseNamespace
from socketio.mixins import RoomsMixin, BroadcastMixin

from app.user import User


class ChatNamespace(BaseNamespace, RoomsMixin, BroadcastMixin):

    autoincrement_id = 0

    def __init__(self, *args, **kwargs):
        super(ChatNamespace, self).__init__(*args, **kwargs)

        self._user = None

    def login(self, is_update=False):
        if is_update:
            self.user.update_vars_from_redis()

        self.request['users'].append(self.user)
        self.socket.session['user'] = self.user

        self.broadcast_event(
            'announcement',
            '%s has connected' % self.user.nickname
        )

        self.on_get_online_users()

        # Just have them join a default-named room
        self.join(settings.DEFAULT_USER_ROOM)

    def on_nickname(self, nickname):
        if self.user:
            return {'error': 'You already authenticated.'}

        nickname = nickname.strip()

        if not nickname:
            return {'error': 'Nickname is required.'}

        self._user = User(
            nickname=nickname,
            namespace=self,
        )

        self.login(is_update=False)

        if self.user.is_member:
            self.user.update_vars_from_redis()
        else:
            self.user.register()

        self.emit('set_cookie', 'uid', self.user.uid)

    def on_get_online_users(self):
        if self.user:
            self.broadcast_event(
                'nicknames',
                [u.nickname for u in self.request['users']]
            )
        else:
            return {'error': 'You is not authenticated.'}

    def recv_disconnect(self):
        if self.user:
            self.user.close(is_clear=False)

            self.request['users'].remove(self.user)

            self.broadcast_event(
                'announcement',
                '%s has disconnected' % self.user.nickname
            )

            self._user = None

            self.on_get_online_users()

        self.disconnect(silent=True)

    def send_to_room(self, msg, room=None):
        if self.user:
            self.emit_to_room(
                (room if room else settings.DEFAULT_USER_ROOM),
                'msg_to_room',
                self.user.nickname,
                msg
            )

    @property
    def user(self):
        if self._user is None:
            self._user = self.socket.session.get('user', None)

            if not self._user:
                nickname = User.get_nickname_by_uid(
                    uid=self.get_from_cookie('uid')
                )

                if nickname:
                    self._user = User(
                        nickname=nickname,
                        namespace=self
                    )

                    self.login(is_update=True)

                else:
                    self.emit('del_cookie', 'uid')
                    self.emit('disconnect')

                    self._user = False

        return self._user

    def on_user_message(self, msg):
        if self.user:
            self.user.process_message(msg=msg)
        else:
            return {'error': 'You is not authenticated.'}

    def get_from_cookie(self, key):
        cookies = self.environ.get('HTTP_COOKIE')
        if not cookies:
            return None

        for cookie in cookies.split(';'):
            cookie = cookie.strip()
            if cookie.startswith('%s=' % key):
                return cookie[4:]
