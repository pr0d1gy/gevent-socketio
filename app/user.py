import hashlib
from app.user_worker import UserWorker


class User(UserWorker):

    def __init__(self, nickname, namespace):
        self._nickname = nickname

        # Generate uid for current user.
        self._uid = hashlib.md5('%s%s%s' % (
            nickname,
            namespace.environ.get('HTTP_USER_AGENT', ''),
            namespace.environ.get('HTTP_X_FORWARDED_FOR', '')
        )).hexdigest()

        super(User, self).__init__(namespace=namespace)

    def __str__(self):
        return 'user_%s' % self.uid

    @property
    def nickname(self):
        return self._nickname

    @property
    def uid(self):
        return self._uid
