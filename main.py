import settings

from gevent import monkey
from socketio.server import SocketIOServer

from app import Application


if __name__ == '__main__':
    monkey.patch_all()

    print 'Listening on port 8080 and on port 843 (flash policy server)'
    SocketIOServer(
        settings.SERVER_LISTENER,
        Application(),
        resource="socket.io",
        policy_server=True,
        policy_listener=settings.SERVER_POLICY_LISTENER
    ).serve_forever()
